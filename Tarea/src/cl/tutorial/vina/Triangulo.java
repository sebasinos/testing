package cl.tutorial.vina;

public class Triangulo {
	private int ladoA;
	private int ladoB;
	private int ladoC;
	
	public Triangulo() {
		super();
	}

	public Triangulo(int ladoA, int ladoB, int ladoC) {
		super();
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
	}

	public int getLadoA() {
		return ladoA;
	}

	public void setLadoA(int ladoA) {
		this.ladoA = ladoA;
	}

	public int getLadoB() {
		return ladoB;
	}

	public void setLadoB(int ladoB) {
		this.ladoB = ladoB;
	}

	public int getLadoC() {
		return ladoC;
	}

	public void setLadoC(int ladoC) {
		this.ladoC = ladoC;
	}
	
	public String tipoTriangulo(){
		if(ladoA == ladoB && ladoA == ladoC){
			return "equilatero";
			
		}
		else if (ladoA != ladoB && ladoA != ladoC && ladoB != ladoC){
			return "escaleno";
			}
		else {
			return "Isosceles";
			
		}
	}
			
	
	public boolean corroboraTriangulo() {
		if (ladoA+ladoB > ladoC && ladoA+ladoC > ladoB && ladoB+ladoC > ladoA) {
			return true;
		}
		else {
			return false;
		}

		
	}
	

}
