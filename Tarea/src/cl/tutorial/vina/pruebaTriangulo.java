package cl.tutorial.vina;

import static org.junit.Assert.*;

import org.junit.Test;

public class pruebaTriangulo {
	
	// Test Unitario 1 - PruebaEquilatero : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son iguales - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaEquilatero() {
		Triangulo triangulo = new Triangulo(7,7,7);
		assertEquals("equilatero", triangulo.tipoTriangulo());
		
	}
	// Test Unitario 2 - PruebaEscaleno : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son distintos - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaEscaleno() {
		Triangulo triangulo = new Triangulo(6,7,8);
		assertEquals("escaleno", triangulo.tipoTriangulo());

	}
	// Test Unitario 3 - Pruebaisoceles1 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son lado a y b iguales y lado c distinto - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaisosceles1() {
		Triangulo triangulo = new Triangulo(7,7,8);
		assertEquals("Isosceles", triangulo.tipoTriangulo());

	}
	// Test Unitario 4 - Pruebaisoceles2 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son lado c y b iguales y lado a distinto - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaisosceles2() {
		Triangulo triangulo = new Triangulo(8,7,7);
		assertEquals("Isosceles", triangulo.tipoTriangulo());

	}
	// Test Unitario 5 - Pruebaisoceles1 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son lado a y c iguales y lado b distinto - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaisosceles3() {
		Triangulo triangulo = new Triangulo(7,8,7);
		assertEquals("Isosceles", triangulo.tipoTriangulo());

	}
	// Test Unitario 6 - PruebaTriangulo1 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son lado a + b mayor al lado C  - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaTriangulo1() {
		Triangulo triangulo = new Triangulo(7,7,13);
		assertEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 7 - PruebaTriangulo1 : se entregan 3 variables que corresponden a cada lado del triangulo
		// los 3 lados son enteros y son lado c + b mayor al lado a  - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaTriangulo2() {
		Triangulo triangulo = new Triangulo(13,7,7);
		assertEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 8 - PruebaTriangulo1 : se entregan 3 variables que corresponden a cada lado del triangulo
		// los 3 lados son enteros y son lado a + c mayor al lado b  - Esta prueba debe ser satisfactoria
	@Test
	public void pruebaTriangulo3() {
		Triangulo triangulo = new Triangulo(7,13,7);
		assertEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 9 - PruebaFailEquilatero : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son distintos - Esta prueba debe ser Fallida
	@Test
	public void pruebaFailEquilatero() {
		Triangulo triangulo = new Triangulo(1,7,6);
		assertNotEquals("equilatero", triangulo.tipoTriangulo());
		
	}
	// Test Unitario 10 - PruebaFailEscaleno : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son Iguales - Esta prueba debe ser Fallida
	@Test
	public void pruebaFailEscaleno() {
		Triangulo triangulo = new Triangulo(7,7,7);
		assertNotEquals("escaleno", triangulo.tipoTriangulo());

	}
	// Test Unitario 11 - PruebaFailisoceles1 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son los 3 lados distinto - Esta prueba debe ser Fallida
	@Test
	public void pruebaFailisosceles1() {
		Triangulo triangulo = new Triangulo(6,7,8);
		assertNotEquals("Isosceles", triangulo.tipoTriangulo());

	}
	// Test Unitario 12 - PruebaFailisoceles2 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son llos 3 lados iguales  - Esta prueba debe ser Fallida
	
	@Test
	public void pruebaFailisosceles2() {
		Triangulo triangulo = new Triangulo(7,7,7);
		assertNotEquals("Isosceles", triangulo.tipoTriangulo());

	}
	// Test Unitario 13 - PruebaFailTriangulo1 : se entregan 3 variables que corresponden a cada lado del triangulo
	// los 3 lados son enteros y son lado a + b menor al lado C  - Esta prueba debe ser fallida
	@Test
	public void pruebaFailTriangulo1() {
		Triangulo triangulo = new Triangulo(7,7,15);
		assertNotEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 14 - PruebaFailTriangulo2 : se entregan 3 variables que corresponden a cada lado del triangulo
		// los 3 lados son enteros y son lado b + c menor al lado a  - Esta prueba debe ser fallida
	@Test
	public void pruebaFailTriangulo2() {
		Triangulo triangulo = new Triangulo(15,7,7);
		assertNotEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 15 - PruebaTriangulo1 : se entregan 3 variables que corresponden a cada lado del triangulo
		// los 3 lados son enteros y son lado a + c menor al lado b  - Esta prueba debe ser fallida
	@Test
	public void pruebaFailTriangulo3() {
		Triangulo triangulo = new Triangulo(7,15,7);
		assertNotEquals(true, triangulo.corroboraTriangulo());

	}
	// Test Unitario 16 - pruebaErrorstr : se entregan 3 variables que corresponden a cada lado del triangulo
			// 2 lados son enteros y uno es Str  - Esta prueba debe ser Erronea
	@Test
	public void pruebaErrorstr() {
		Triangulo triangulo = new Triangulo("a",15,7);
		assertNotEquals(true, triangulo.corroboraTriangulo());

	}
}
