#include "pch.h"
#include "../testImc/CalcImc.cpp"
#include <string>


TEST(ImcTest, CheckValordelCalculo) {
	float esperado = 27.69;
	float peso = 80;
	float altura = 1.70;
	ASSERT_EQ(esperado, calculoImc(altura, peso));
}
TEST(ImcTest2, CheckValorerroneoDelCalculo) {
	float esperado = 24;
	float peso = 80;
	float altura = 1.70;
	ASSERT_NE(esperado, calculoImc(altura, peso));
}
TEST(ImcTest3, CheckValorporCero) {
	float esperado = 0.0;
	ASSERT_NE(esperado, checkPeso(90));
}
TEST(ImcTest4, CheckValorValporCero) {
	float esperado = 0.0;
	ASSERT_NE(esperado, checkalt(1.80));
}
TEST(ImcTest5, CheckStrCondValMasBajo) {
	const char* msg  = "Peligro, estas bajo peso";
	EXPECT_STREQ(msg, condicion(0.1) );
}
TEST(ImcTest6, CheckStrCondValMedio) {
	const char* msg = "Peligro, estas bajo peso";
	EXPECT_STREQ(msg, condicion(10));
}
TEST(ImcTest7, CheckStrCondValMasAlto) {
	const char* msg = "Peligro, estas bajo peso";
	EXPECT_STREQ(msg, condicion(19.9));
}
TEST(ImcTest8, CheckStrCondValMasBajoErroneo) {
	const char* msg = "Peligro, estas bajo peso";
	EXPECT_STRNE(msg, condicion(20));
}
TEST(ImcTest9, CheckStrCondValMasBajo) {
	const char* msg2 = "Felicidades, estas en tu peso ideal";
	EXPECT_STREQ(msg2, condicion(20));
}
TEST(ImcTest10, CheckStrCondValMedio) {
	const char* msg2 = "Felicidades, estas en tu peso ideal";
	EXPECT_STREQ(msg2, condicion(22.5));
}
TEST(ImcTest11, CheckStrCondValMasAlto) {
	const char* msg2 = "Felicidades, estas en tu peso ideal";
	EXPECT_STREQ(msg2, condicion(24.9));
}
TEST(ImcTest12, CheckStrCondValMasBajoErroneo) {
	const char* msg2 = "Felicidades, estas en tu peso ideal";
	EXPECT_STRNE(msg2, condicion(25));
}
TEST(ImcTest13, CheckStrCondValMasBajo) {
	const char* msg3 = "Tienes sobrepeso";
	EXPECT_STREQ(msg3, condicion(25));
}
TEST(ImcTest14, CheckStrCondValMedio) {
	const char* msg3 = "Tienes sobrepeso";
	EXPECT_STREQ(msg3, condicion(27.5));
}
TEST(ImcTest15, CheckStrCondValMasAlto) {
	const char* msg3 = "Tienes sobrepeso";
	EXPECT_STREQ(msg3, condicion(29.9));
}
TEST(ImcTest16, CheckStrCondValMasBajoErroneo) {
	const char* msg3 = "Tienes sobrepeso";
	EXPECT_STRNE(msg3, condicion(30));
}
TEST(ImcTest17, CheckStrCondValMasBajo) {
	const char* msg4 = "Estas Obeso";
	EXPECT_STREQ(msg4, condicion(30));
}
TEST(ImcTest18, CheckStrCondValMedio) {
	const char* msg4 = "Estas Obeso";
	EXPECT_STREQ(msg4, condicion(32.5));
}
TEST(ImcTest19, CheckStrCondValMasAlto) {
	const char* msg4 = "Estas Obeso";
	EXPECT_STREQ(msg4, condicion(34.9));
}
TEST(ImcTest20, CheckStrCondValMasBajoErroneo) {
	const char* msg4 = "Estas Obeso";
	EXPECT_STRNE(msg4, condicion(35));
}
TEST(ImcTest21, CheckStrCondValMasBajo) {
	const char* msg5 = "Peligro, Tienes obesidad Morbida";
	EXPECT_STREQ(msg5, condicion(35));
}
TEST(ImcTest22, CheckStrCondValMasaltoHipotetic) {
	const char* msg5 = "Peligro, Tienes obesidad Morbida";
	EXPECT_STREQ(msg5, condicion(1000000000));
}
TEST(ImcTest23, CheckStrCondValorMasBajoErroneo) {
	const char* msg5 = "Peligro, Tienes obesidad Morbida";
	EXPECT_STRNE(msg5, condicion(34.9));
}